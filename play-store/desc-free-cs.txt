----------
| Title: |
----------
GPX Viewer - stopy, trasy a trasové body



-------------------------------------------------------------------------------------
| Short description (maximum allowed length in Google Play Store is 80 characters): |
-------------------------------------------------------------------------------------
GPX Viewer zobrazuje prošlé stopy, trasy a trasové body gpx a kml souborů.



----------------
| Description: |
----------------
Získejte o mnoho více funkcí, než jen prohlížení gpx, kml, kmz a loc souborů. Zjistěte, proč jsme jednou z nejlépe hodnocených aplikací pro offline vektorové mapy. GPX Viewer je dokonalý GPS lokátor, prohlížeč, analyzátor, záznamník GPS a jednoduchý navigační nástroj pro vaše výlety a outdoorové aktivity.

<b>GPX, KML, KMZ A LOC</b>
• prohlížejte prošlé stopy, trasy a trasové body ze souborů gpx, kml, kmz a loc
• objevte prohlížeč souborů, který otevírá více fotmátů a podporuje oblíbené soubory a historii
• komprimujte soubory gpx do gpz a soubory kml do kmz (zip archivy)

<b>PODROBNÉ STATISTIKY CESTY</b>
• analyzujte informace a statistiky ze stop a tras
• prohlížejte grafy, jako je výškový a rychlostní profil pro stopy a trasy
• zobrazte si grafy dalších údajů o trase, jako je kadence, tepová frekvence, výkon a teplota vzduchu
• analyzujte informace o trasových bodech a upravte si jejich ikony
• změňte si barvu stopy, trasy
• nastavte si barvu stopy či trasy podle nadmořské výšky, rychlosti, kadence, tepové frekvence nebo teploty vzduchu

<b>ONLINE MAPY</b>
• nastavte si online mapy, jako jsou Google Maps, Mapbox, HERE, Thunderforest a některé další, založené na datech OpenStreetMap, náhled: https://go.vecturagames.com/online (Mapbox, HERE a online mapy Thunderforest je třeba zakoupit)
• zobrazte si vrstvy pro počasí OpenWeatherMap (nutno zakoupit)
• přidejte své vlastní online mapy TMS nebo WMS

<b>JEDNODUCHÁ NAVIGACE</b>
• zobrazuje aktuální GPS polohu na mapě
• průběžně sleduje polohu GPS na mapě
• otáčí mapu podle orientačního senzoru zařízení nebo podle údajů o směru pohybu z GPS
• díky funkcím sledování GPS polohy a otáčení mapy lze GPX Viewer používat jako jednoduchou navigaci
• upozorní s nastavitelnou vzdáleností, když je GPS pozice blízko trasového bodu

<b>INTEGRACE S TRACKBOOK</b>
• synchronizace tras a trasových bodů vytvořených v aplikaci Trackbook - https://trackbook.online

---------

GPX Viewer je vysoce přizpůsobitelný. Vše si nastavíte podle svých potřeb!

Pokud chcete prohlížeč gpx bohatý na funkce, který je také nástrojem s jednoduchou navigací, online mapami, GPS lokátorem, prohlížečem tras GPS, prohlížečem statistik výletů a má další užitečné funkce, GPX Viewer je pro to nejlepší aplikace!



----------------------------
| Screenshot descriptions: |
----------------------------
1. Zobrazení stop, tras a trasových bodů ze souborů gpx a kml
2. Prohlížení stop a tras
3. Analýza stop
4. Zobrazení trasových bodů
5. Spousta typů map, OpenWeatherMap, vlastní mapy WMS
6. Informace a statistiky o stopách, trasách a trasových bodech
7. Grafy nadmořské výšky, rychlosti, kadence, tepové frekvence a výkonu
8. Prohlížeč souborů, který otevírá vícero souborů současně
