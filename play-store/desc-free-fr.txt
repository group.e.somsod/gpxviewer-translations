﻿----------
| Title: |
----------
GPX Viewer - Pistes, routes et points



-------------------------------------------------------------------------------------
| Short description (maximum allowed length in Google Play Store is 80 characters): |
-------------------------------------------------------------------------------------
GPX Viewer affiche les pistes, routes et points depuis des fichiers gpx et kml.


----------------
| Description: |
----------------
Affichez les fichiers gpx, kml, kmz, loc, et obtenez encore plus de fonctionnalités. Découvrez pourquoi nous sommes l’une des apps de cartes en ligne les mieux notées. GPX Viewer est le traceur GPS ultime: il vous permet de visualiser les pistes GPS, d’analyser, suivre en temps réel. Vous bénéficiez d’un outil de navigation simple et complet pour vos voyages et vos activités de plein air.

<b>GPX, KML, KMZ AND LOC</b>
• Visualisez chemins, itinéraires et points de repère dans des fichiers gpx, kml, kmz et loc 
• Bénéficiez d’un navigateur qui ouvre plusieurs fichiers et prend en charge l’historique et les favoris
• Compressez les fichiers gpx files en gpz et le kml en kmz (archives zip)

<b>DES STATISTIQUES DE VOYAGE DETAILLEES</b>
• Analyse de l’information et des statistiques des itinéraires et chemins
• Obtenez des graphiques de données tels que l’altitude ou la vitesse sur vos itinéraires et chemins
• Obtenez des graphiques et autres données tels que la cadence, le rythme cardiaque, la température extérieure
• Analysez les informations pour les points de repère et ajustez les icônes
• Modifiez les couleurs des pistes et itinéraires
• Colorez les chemins et itinéraires en fonction de l’altitude, la vitesse, la cadence, le rythme cardiaque et la température extérieure

<b>DES CARTES EN LIGNE</b>
• Des cartes connectées telles que Google Maps, Mapbox, HERE, Thunderforest et d’autres, tirées de l’OpenStreetMap data, prévisualisation: https://go.vecturagames.com/online (les cartes en ligne Mapbox, HERE et Thunderforest doivent être achetées)
• OpenWeatherMap pour les calques de couleurs en fonction des conditions météorologiques (doit être acheté)
• Personnalisez en ligne les cartes TMS or WMS

<b>UN OUTIL DE NAVIGATION SIMPLE</b>
• Montre votre situation GPS sur une carte
• Suit votre position GPS de façon continue en ajustant la position de la carte sur l’écran
• Pivote la carte en fonction de votre smartphone ou tablette ou de la direction des données du GPS
• Avec ce suivi GPS et les fonctionnalités de pivot de la carte, GPX Viewer peut être utilisé comme un simple outil de navigation
• Notification envoyées quand la position GPS est proche du point de repère avec une distance ajustable

<b>INTÉGRATION DU TRACKBOOK</b>
• Synchroniser les traces et les points de cheminement créés sur le Trackbook - https://trackbook.online

---------

GPX Viewer est finement paramétrable. Vous pouvez l’ajuster en fonction de vos besoins!

Si vous souhaitez des fonctionnalités poussées, des cartes en ligne, des outils de navigation simples et performants, un traceur GPS, un GPS pour les pistes, des statistiques de parcours et d’autres fonctionnalités utiles GPX Viewer est la meilleure app pour vous!



----------------------------
| Screenshot descriptions: |
----------------------------
1. Visualisez les pistes, routes et points depuis des fichiers gpx et kml
2. Affiche les pistes et routes
3. Analyse de piste
4. Affiche les points de cheminements
5. De nombreux types de cartes, OpenWeatherMap, cartes WMS personnalisées
6. Des infos et des stats sur les pistes, routes et points de cheminement
7. Des graphes d’élévation, vitesse, cadence, rythme cardiaque et puissance
8. Un explorateur pour ouvrir plusieurs fichiers
