﻿----------
| Title: |
----------
GPX Viewer - Banen, routes en tussenpunten



-------------------------------------------------------------------------------------
| Short description (maximum allowed length in Google Play Store is 80 characters): |
-------------------------------------------------------------------------------------
GPX Viewer toont banen, trajecten en tussenpunten in gpx en kml formaat.



----------------
| Description: |
----------------
Bekijk gpx, kml, kms, loc bestanden maar met meer mogelijkheden. Ontdek waarom wij één van de beste kaarten app zijn. GPX Viewer is de ultieme tool voor GPS locatie, GPS traject bekijken, analyseren, opnemen en eenvoudige navigatie voor je buitenhuis trips en activiteiten.

<b>GPX, KML, KMZ AND LOC</b>
• bekijk trajecten, routes en tussenpunten van gpx, kml, kms en loc bestanden
• bestanden verkenner opent automatisch meerdere bestanden en ondersteunt je favoriete bestanden en je historiek
• verklein je gpx-bestanden in gpz en kml bestanden in kmz (ZIP archieven)

<b>DETAIL REIS STATISTIEK</b>
• analyseer info en statistieken voor trajecten en routes
• bekijk grafieken (kaarten) zoals stijgingsprofiel en snelheidsprofiel van trajecten en routes
• bekijk grafieken van andere trajectgegevens zoals tempo, hartslag, kracht en omgevingstemperatuur
• analyseer informatie van tussenpunten en pas de iconen ervan aan
• wijzig traject- en routekleur
• kleur traject- en routelijnen met de stijging, snelheid, hartslag of omgevingstemperatuur

<b>ONLINE KAARTEN</b>
• online kaarten zoals Google Maps, Mapbox, HERE, Thunderforest en sommige andere gebaseerd op OpenStreetMap gegevens, voorbeeld: https://go.vecturagames.com/online (Mapbox, HERE en Thunderforest online kaarten moeten worden gekocht)
• OpenWeatherMap met weer-lagen en overlays (moet worden gekocht)
• voeg je eigen online TMS of WMS kaarten toe

<b>EENVOUDIGE NAVIGATIE TOOL</b>
• toon huidige GPS positie op de kaart
• volg GPS positie voortdurend en pas de kaartpositie aan
• draai de kaart volgens de oriëntatie-sensor van het toestel of volgens de rijrichting volgens de GPS
• bij GPS volgen en de draai kaart optie is GPX Viewer een een voudige navigatie tool
• berichten wanneer de GPS-positie in de buurt van een tussenpunt komt met aanpasbare afstand

<b>TRACKBOOK-INTEGRATIE</b>
• synchroniseer banen en tussenpunten gemaakt op Trackbook - https://trackbook.online

---------

GPX Viewer is verregaand aanpasbaar. Je kan alles volgens je eigen wensen instellen!

Wens je een gpx viewer met vele opties die daarnaast ook nog een navigatie tool is, online kaarten, GPS locatie, GPS Traject viewer, rit statistieken en andere nuttige opties heeft dan is GPX Viewer de beste app!



----------------------------
| Screenshot descriptions: |
----------------------------
1. Toont banen, trajecten en tussenpunten van gpx, kml, kmz en loc bestanden
2. Toont banen en trajecten
3. Traject analyse
4. Toont tussenpunten
5. Veel kaart-types, OpenWeatherMap, aangepaste WMS kaarten
6. Info en statistieken over banen, trajecten en tussenpunten
7. Grafiek van stijging, snelheid, tempo, hartslag en vermogen
8. Bestand browser kan meerdere bestanden openen
