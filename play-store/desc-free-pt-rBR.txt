﻿----------
| Title: |
----------
GPX Viewer - Trilhas, rotas e pontos de via



-------------------------------------------------------------------------------------
| Short description (maximum allowed length in Google Play Store is 80 characters): |
-------------------------------------------------------------------------------------
GPX Viewer exibe trilhas, rotas e pontos de via de arquivos gpx e kml.



----------------
| Description: |
----------------
Visualize arquivos gpx, kml, kmz, loc, mas obtenha muito mais recursos. Veja por que nós somos um dos aplicativos de mapas on-line melhores avaliados. O GPX Viewer é o localizador GPS definitivo, visualizador de trilhas de GPS, analisador e ferramenta de navegaçăo simples para suas viagens e atividades ao ar livre.

<b>GPX, KML, KMZ e LOC</b>
• visualize trilhas, rotas e pontos de via de arquivos gpx, kml, kmz e loc
• navegador de arquivos que abre vários arquivos e tem suporte para arquivos avoritos e histórico
• comprima arquivos gpx em arquivos gpz e kml em kmz (arquivos zip)

<b>ESTATÍSTICA DE VIAGEM DETALHADA</b>
• analise informaçőes e estatísticas para trilhas e rotas
• visualize gráficos (tabelas) como perfil de elevaçăo e perfil de velocidade para trilhas e rotas
• visualize gráficos de outros dados de trilha como cadęncia, frequęncia cardíaca, potęncia e temperatura do ar
• analise informaçőes para pontos de via e ajuste seus ícones
• altere cor de trilha e rota
• colora trilha e rota linha por elevaçăo, velocidade, cadęncia, ritmo cardíaco ou temperatura do ar

<b>MAPAS ON-LINE</b>
• mapas on-line como o Google Maps, Mapbox, HERE, Thunderforest e alguns outros baseados em dados do OpenStreetMap, pré-visualizaçăo: https://go.vecturagames.com/online (é necessário adquirir os mapas online Mapbox, HERE e Thunderforest)
• camadas e superposiçőes de clima OpenWeatherMap (precisa ser comprado)
• adicione seus mapas TMS ou WMS on-line personalizados

<b>FERRAMENTA DE NAVEGAÇĂO SIMPLES</b>
• mostra a posiçăo atual do GPS em um mapa
• siga a posiçăo GPS continuamente, ajustando a posiçăo do mapa
• gire o mapa de acordo com o sensor de orientaçăo do dispositivo ou de acordo com dados de direçăo de movimento do GPS
• com a posiçăo de seguir GPS e rotaçăo dos recursos do mapa, o GPX Viewer pode ser usado como uma ferramenta de navegaçăo simples
• notificaçăo quando a posiçăo do GPS está perto do ponto de via com distância ajustável

<b>INTEGRAÇÃO DE TRACKBOOK</b>
• sincronizar trilhas e pontos de via criados no Trackbook - https://trackbook.online

---------

O GPX Viewer é altamente personalizável. Vocę pode definir tudo de acordo com suas necessidades!

Se vocę quer um visualizador gpx rico em recursos, que também é uma ferramenta com navegaçăo simples, mapas on-line, localizador GPS, visualizador de trilhas de GPS, visualizador de estatísticas de viagem e tem outros recursos úteis, GPX Viewer é o melhor aplicativo para isso!



----------------------------
| Screenshot descriptions: |
----------------------------
1. Visualize trilhas, rotas e pontos de via de arquivos gpx e kml
2. Mostrar faixas e rotas
3. Análise de trilhas
4. Mostrar pontos de via
5. Muitos tipos de mapas, OpenWeatherMap, mapas WMS personalizados
6. Informações e estatísticas sobre trilhas, rotas e pontos de via
7. Gráficos de elevação, velocidade, cadência, freqüência cardíaca e potência
8. Navegador de arquivos que abre vários arquivos
