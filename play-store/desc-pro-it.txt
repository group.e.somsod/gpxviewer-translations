﻿----------
| Title: |
----------
GPX Viewer PRO - Tracce, Rotte e Waypoint



-------------------------------------------------------------------------------------
| Short description (maximum allowed length in Google Play Store is 80 characters): |
-------------------------------------------------------------------------------------
GPX Viewer PRO visualizza tracce, rotte e waypoint da file gpx e kml.



----------------
| Description: |
----------------
Visualizza file gpx, kml, kmz, loc, con tante funzioni in più. Scopri perché siamo una delle app di mappe vettoriali offline con migliore valutazione. GPX Viewer PRO è il miglior localizzatore GPS, visualizzatore di tracce GPS, analizzatore, registratore, tracciatore e semplice strumento di navigazione per i tuoi viaggi e attività all'aperto.

<b>GPX, KML, KMZ E LOC</b>
• visualizza tracce, rotte e waypoint da file gpx, kml, kmz e loc
• browser in grado di aprire molteplici file con supporto per preferiti e cronologia
• compressione file gpx in gpz e file kml in kmz (archivi zip)

<b>STATISTICHE DETTAGLIATE</b>
• analizza informazioni e statistiche di tracce e rotte
• visualizza grafici quali curve di altitudine e velocità per tracce e rotte
• visualizza grafici per altri dati della traccia quali ritmo, frequenza cardiaca, energia, temperatura esterna
• analizza le informazioni dei waypoint e modifica le loro icone
• modifica colore di traccia e rotta
• colora traccia e rotta in base ai parametri di altitudine, velocità, ritmo, frequenza cardiaca o temperatura esterna

<b>MAPPE ONLINE</b>
• mappe online come Google Maps, Mapbox, HERE, Thunderforest ed altre basate su dati OpenStreetMap, anteprima: https://go.vecturagames.com/online
• livelli OpenWeatherMap
• aggiungi le tue mappe online TMS o WMS personalizzate

<b>SEMPLICE STRUMENTO DI NAVIGAZIONE</b>
• mostra posizione GPS in tempo reale sulla mappa
• segui la posizione GPS in tempo reale aggiustando la posizione della mappa
• rotazione della mappa in base al sensore di orientamento del dispositivo o alla direzione rilevata dal GPS
• grazie alla funzione di inseguimento e rotazione della mappa GPX Viewer PRO può essere utilizzato come semplice strumento di navigazione
• notifica quando la posizione GPS si trova nelle vicinanze di un waypoint, con distanza regolabile

<b>INTEGRAZIONE DEL TRACKBOOK</b>
• sincronizzare le tracce e i waypoint creati su Trackbook - https://trackbook.online

<b>MAPPE OFFLINE (SOLO VERSIONE PRO)</b>
• mappe vettoriali offline dettagliate con copertura mondiale basate su dati OpenStreetMap
• larga scelta di temi per mappe offline dedicate alla città o alle attività all'aperto, anteprima: https://go.vecturagames.com/offline
• aggiornamenti mensili con miglioramenti nei dati

<b>REGISTRAZIONE TRACCIA (SOLO VERSIONE PRO)</b>
• registra ed esporta i tuoi percorsi in file gpx o kml
• registra statistiche relative ad altitudine e velocità
• profili di registrazione modificabili per diverse attività all'aperto
• notifica vocale per distanza o tempo

<b>PREVISIONI METEO (SOLO VERSIONE PRO)</b>
• previsioni meteo per i prossimi 7 giorni
• mostra previsioni orarie

---------

GPX Viewer PRO è estremamente personalizzabile. Puoi impostare ogni cosa secondo le tue esigenze!

Se desideri un visualizzatore per gpx ricco di funzioni, che sia anche un'app per mappe offline e strumento con funzioni base di navigazione, mappe vettoriali offline, localizzatore GPS, visualizzatore tracce GPS, visualizzatore di statistiche e altri utili caratteristiche, GPX Viewer PRO è l'app migliore per tutto questo!



----------------------------
| Screenshot descriptions: |
----------------------------
1. Visualizza tracce, rotte e waypoint da file gpx e kml
2. Mappe vettoriali offline basate su OpenStreetMap
3. Registrazione di tracce
4. Mostra tracce e rotte
5. Analisi della traccia
6. Mostra i waypoint
7. Numerosi tipi di mappa, OpenWeatherMap, mappe WMS personalizzate
8. Grafici relativi ad altitudine, velocità, ritmo, frequenza cardiaca
